//
//  FeedData.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 4/15/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import Foundation

public protocol FeedHeaderData {
    var userName: String? { get }
    var userImageUrl: String? { get }
    var date: Date? { get }
}

public protocol FeedDescriptionData {
    var description: String? { get }
    var descriptionBase64: String? { get }
    var mentions: [MentionLink] { get }
}

public protocol FeedMediaData {
    var imageUrl: String? { get }
    var videoUrl: String? { get }
}

public protocol FeedLikesData {
    var numberOfLikes: Int? { get }
    var userLikeNames: [String]? { get }
}

public protocol FeedCommentData {
    var userName: String? { get }
    var userImageUrl: String? { get }
    var date: Date? { get }
    var comment: String? { get }
    var commentBase64: String? { get }
    var mentions: [MentionLink] { get }
}

public protocol FeedData {
    var headerData: FeedHeaderData? { get }
    var descriptionData: FeedDescriptionData? { get }
    var mediaData: FeedMediaData? { get }
    var likesData: FeedLikesData? { get }
    var commentsData: [FeedCommentData]? { get }
    var maxComments: Int? { get }
}

