//
//  FeedCacheDataSource.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 6/11/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import Foundation

public class FeedCacheSource {
    
    struct CacheData {
        var feedConfig: [FeedTableViewController.CellConfiguration]
        var isValidConfig: Bool
        var commentPosition: Int
        var isValidComment: Bool
    }
    
    internal weak var feedTableViewController: FeedTableViewController?
    private var cache: [CacheData] = []
    
    //Mark: Public Interface
    
    public init() {
    }
    
    /// Invalidates the current cache, deleting all its objects
    public func invalidateCache() {
        self.cache = []
    }
    
    /// Invalidate the cache from a specific feed post
    ///
    /// - Parameters:
    ///   - index: Position of the post on the feed list
    ///   - shouldRemove: True if the post cache should be deleted
    public func invalidateCache(at index: Int, shouldRemove: Bool? = false) {
        if let shouldRemove = shouldRemove, shouldRemove {
            self.cache.remove(at: index)
            return
        }
        guard self.cache.count < index else { return }
        self.cache[index].isValidConfig = false
        self.cache[index].isValidComment = false
    }
    
    /// Method for inserting cache placeholder for a feed that just was inserted into the
    /// feed list.
    ///
    /// - Parameter index: Index where the new Feed should be inserted on the list.
    public func insertCachePlaceholder(at index: Int) {
        self.cache.insert(CacheData.init(feedConfig: [], isValidConfig: false, commentPosition: 0, isValidComment: false), at: index)
    }
    
    /// Append the cache data generated to the current one.
    ///
    /// - Parameter feedData: Feed data from where the cache should be generated
    public func processIncrementRawData(feedData: [FeedData]) {
        DispatchQueue.global(qos: .userInteractive).sync {
            self.constructCache(with: feedData)
        }
    }
    
    /// Deletes current cache and generate a new one based on the data
    ///
    /// - Parameter feedData: Feed Data used to generate the new cache
    public func remakeCache(feedData: [FeedData]) {
        DispatchQueue.global(qos: .userInteractive).sync {
            self.invalidateCache()
            self.constructCache(with: feedData)
        }
    }
    
    //MARK: - API Internal Interface
    
    internal func getCachedCellConfigFor(section: Int, row: Int) -> FeedTableViewController.CellConfiguration? {
        guard section < cache.count else {
            return nil
        }
        guard cache[section].isValidConfig else {
            return nil
        }
        guard row < cache[section].feedConfig.count else {
            return nil
        }
        return cache[section].feedConfig[row]
    }
    
    internal func getFirstCommentPositionFor(section: Int) -> Int? {
        guard section < cache.count else {
            return nil
        }
        guard cache[section].isValidComment else {
            return nil
        }
        return cache[section].commentPosition
    }
    
    internal func updateCacheAt(index: Int, with feedConfig: [FeedTableViewController.CellConfiguration]) {
        guard index < cache.count else {
            return
        }
        self.cache[index].feedConfig = feedConfig
    }
    
    internal func updateCommentPostitionAt(index: Int, with position: Int) {
        guard index < cache.count else {
            return
        }
        self.cache[index].commentPosition = position
    }
    
    //MARK: - Private Methods
    
    private func constructCache(with feedData: [FeedData]) {
        for feed in feedData {
            //Caching the cell configuration
            if let config = self.feedTableViewController?.expandedConfiguration(feed: feed) {
                let commentPosition = self.feedTableViewController?.firstCommentPosition(feed: feed) ?? 0
                let cacheData = CacheData(feedConfig: config, isValidConfig: true, commentPosition: commentPosition, isValidComment: true)
                self.cache.append(cacheData)
            } else {
                self.cache.append(CacheData(feedConfig: [], isValidConfig: false, commentPosition: 0, isValidComment: false))
            }
        }
    }
    
}

