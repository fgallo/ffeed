//
//  FeedHelper.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 4/16/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import Foundation
import UIImageViewAligned

/// Dates options for Feed date format
///
/// - intervalAgo: 2d, 2m, 2h, 2M,
/// - friendlyDate: Agora mesmo, 2 minutos, 2 horas, 2 dias, >7 dias (d MMM yyyy)
/// - numericDate: dd/MM/YYYY
/// - custom: Format should be specified with a string
public enum DateLabelFormat {
    case intervalAgo
    case friendlyDate
    case numericDate
    case custom(String)
}

internal class FeedHelper {
    
    static func stringDateFrom(date: Date, withFormat format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    static func configureImageView(imageView: UIImageView, imageUrl: String?) {
        guard let imageUrl = imageUrl else {
            return
        }
        guard let url = URL(string: imageUrl) else {
            return
        }
        imageView.image = nil
        imageView.setImageWithUrl(url)
    }
    
    static func configureImageView(imageView: UIImageViewAligned, imageUrl: String?) {
        imageView.alignment = UIImageViewAlignmentMaskTop
        guard let imageUrl = imageUrl else {
            return
        }
        guard let url = URL(string: imageUrl) else {
            return
        }
        imageView.image = nil
        imageView.setImageWithUrl(url)
    }
    
    static func configureDateLabel(label: UILabel, date: Date, format: DateLabelFormat?) {
        if let format = format {
            switch format {
            case .friendlyDate:
                label.text = date.friendlyDate()
                
            case .intervalAgo:
                label.text = date.getElapsedInterval()
            case .numericDate:
                label.text = FeedHelper.stringDateFrom(date: date, withFormat: "dd/MM/YYYY")
                
            case .custom(let formatString):
                label.text = FeedHelper.stringDateFrom(date: date, withFormat: formatString)
            }
        }   else {
            label.text = date.getElapsedInterval()
        }
    }
    
    static func decodeString(string64: String) -> String? {
        if let data = Data(base64Encoded: string64) {
            let decodedString = String(data: data, encoding: .utf8)
            return decodedString
        }
        return nil
    }
    
}

