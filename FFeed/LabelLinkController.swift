//
//  LabelLinkController.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 4/16/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import Foundation
import TTTAttributedLabel

/// Link types supported for automatic highlighting
///
/// - none: no link support
/// - usersName: support to links of the format @username
/// - hashTags: support to links of type #something
/// - webLinks: support to webLinks that can be oppened on Iphone
public enum LinkTypes {
    case none
    case usersName
    case hashTags
    case webLinks
}

internal protocol LabelLinkControllerDelegate : class {
    func didSelectLink(label: UILabel, url: URL)
}

public protocol MentionLink {
    var mentionName: String { get }
    var mentionId: String { get }
}

internal class LabelLinkController: NSObject, TTTAttributedLabelDelegate {
    
    static let sharedInstance = LabelLinkController()
    weak var delegate: LabelLinkControllerDelegate?
    
    var config: [LinkTypes]
    
    private override init() {
        config = []
    }
    
    public func addLinkToFirst(label: inout TTTAttributedLabel, mention: MentionLink) {
        //First get the text from the label and try to match the whole string
        var text = label.text! as! NSString
        let range = text.range(of: mention.mentionName)
        if range.location != NSNotFound {
            text = text.replacingCharacters(in: range, with: "") as NSString
            label.addLink(to: URL(string: "::user::\(mention.mentionId)")!, with: range)
        }
    }
    
    private func addPrefixLinks(label: inout TTTAttributedLabel, prefix: String) {
        var text = label.text! as! NSString
        let words = text.components(separatedBy: .whitespacesAndNewlines)
        
        let userTags = words.filter { $0.hasPrefix(prefix)}
        
        for tag in userTags {
            let range = text.range(of: tag)
            text = text.replacingCharacters(in: range, with: "") as NSString
            label.addLink(to: URL(string: tag), with: range)
        }
    }
    
    private func addWebLinks(label: inout TTTAttributedLabel) {
        let text = label.text! as! String
        let pat = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        
        do {
            let regex = try NSRegularExpression(pattern: pat, options: [])
            let matches = regex.matches(in: text, options: [], range: NSRange.init(location: 0, length: text.count))
            
            for match in matches {
                let urlString = (text as NSString).substring(with: match.range)
                label.addLink(to: URL(string: urlString), with: match.range)
            }
            
        } catch {
            return
        }
    }
    
    func createLinks(label: inout TTTAttributedLabel) {
        guard config.count > 0 else {
            return
        }
        
        label.delegate = self
        
        if config.contains(.usersName) {
            addPrefixLinks(label: &label, prefix: "@")
        }
        
        if config.contains(.hashTags) {
            addPrefixLinks(label: &label, prefix: "#")
        }
        
        if config.contains(.webLinks) {
            addWebLinks(label: &label)
        }
    }
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        delegate?.didSelectLink(label: label, url: url)
    }
    
}
