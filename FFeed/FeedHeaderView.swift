//
//  FeedHeaderView.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 4/15/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import Foundation

public protocol FeedHeaderView {
    var userImageView: UIImageView? { get  set }
    var userNameLabel: UILabel? { get  set }
    var dateLabel: UILabel? { get set }
    var dateLabelFormat: DateLabelFormat? { get set }
}

extension FeedHeaderView {
    
    func configure(data: FeedHeaderData) {
        if let userImageView = userImageView {
            FeedHelper.configureImageView(imageView: userImageView, imageUrl: data.userImageUrl)
        }
        
        if let dateLabel = dateLabel, let date = data.date {
            FeedHelper.configureDateLabel(label: dateLabel, date: date, format: dateLabelFormat)
        }
        
        userNameLabel?.text = data.userName ?? ""
    }
}

