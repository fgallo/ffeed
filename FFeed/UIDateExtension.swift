//
//  UIDateExtension.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 4/16/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import Foundation

extension Date {
    func getElapsedInterval() -> String {
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
        
        if let year = interval.year, year > 0 {
            return "\(year)a"
        } else if let month = interval.month, month > 0 {
            return "\(month)m"
        } else if let day = interval.day, day > 0 {
            return "\(day)" + "d"
        } else if let hour = interval.hour, hour > 0 {
            return "\(hour)" + "h"
        } else if let minute = interval.minute, minute > 0 {
            return "\(minute)" + "min"
        } else if let second = interval.second, second > 0 {
            return "\(second)" + "s"
        } else {
            return "agora"
        }
        
    }
    
    func friendlyDate() -> String {
        
        let secondsAgo = Int(Date().timeIntervalSince(self))
        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        
        if secondsAgo <= minute {
            return "Agora mesmo"
        } else if secondsAgo < hour {
            return "\(secondsAgo / minute) \(secondsAgo / minute == 1 ? "minuto" : "minutos") atrás"
        } else if secondsAgo < day {
            return "\(secondsAgo / hour) \(secondsAgo / hour == 1 ? "hora" : "horas") atrás"
        } else if secondsAgo < week {
            return "\(secondsAgo / day) \(secondsAgo / day == 1 ? "dia" : "dias")  atrás"
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM yyyy"
        
        return dateFormatter.string(from: self)
        
    }
    
}
