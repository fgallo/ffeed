//
//  FeedCommentView.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 4/15/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import Foundation
import TTTAttributedLabel

public protocol FeedCommentView {
    var userImageView: UIImageView? { get  set }
    var userNameLabel: UILabel? { get  set }
    var dateLabel: UILabel? { get set }
    var dateLabelFormat: DateLabelFormat? { get set }
    var commentsLabel: TTTAttributedLabel? { get set }
}

extension FeedCommentView {
    private var labelLinkController: LabelLinkController {
        return LabelLinkController.sharedInstance
    }
    
    mutating func configure(data: FeedCommentData) {
        if let userImageView = userImageView {
            FeedHelper.configureImageView(imageView: userImageView, imageUrl: data.userImageUrl)
        }
        
        if let dateLabel = dateLabel, let date = data.date {
            FeedHelper.configureDateLabel(label: dateLabel, date: date, format: dateLabelFormat)
        }
        userNameLabel?.text = data.userName
        
        if commentsLabel != nil {
            
            if let commentBase64 = data.commentBase64 {
                commentsLabel?.text = FeedHelper.decodeString(string64: commentBase64)
            } else {
                commentsLabel?.text = data.comment
            }
            for mention in data.mentions {
                labelLinkController.addLinkToFirst(label: &commentsLabel!, mention: mention)
            }
            labelLinkController.createLinks(label: &commentsLabel!)
        }
        
    }
    
}

