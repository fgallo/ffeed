//
//  FeedMediaView.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 4/15/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import Foundation
import UIImageViewAligned

public protocol FeedMediaView {
    var mediaImageView: UIImageViewAligned? { get set }
}

extension FeedMediaView {
    
    func configure(data: FeedMediaData, previewEnabled: Bool) {
        guard let imageView = mediaImageView else {
            return
        }
        FeedHelper.configureImageView(imageView: imageView, imageUrl: data.imageUrl)
        if previewEnabled {
            //Configure the fullScreen Image Viewer
        }
    }
    
}
