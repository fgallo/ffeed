//
//  UIImageViewExtension.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 4/15/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension UIImageView {
    
    func setImageWithUrl(_ url: URL) {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.frame.size = CGSize(width: 44.0, height: 44.0)
        activityIndicator.activityIndicatorViewStyle = .gray
        activityIndicator.startAnimating()
        self.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint(item: activityIndicator,
                           attribute: .centerX,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .centerX,
                           multiplier: 1.0,
                           constant: 0).isActive = true
        
        NSLayoutConstraint(item: activityIndicator,
                           attribute: .centerY,
                           relatedBy: .equal,
                           toItem: self,
                           attribute: .centerY,
                           multiplier: 1.0,
                           constant: 0).isActive = true
        
        self.sd_setImage(with: url) { (image, error, cacheType, url) in
            if let image = image {
                self.image = image
            }
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
    
}
