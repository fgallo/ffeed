//
//  FeedViewProtocols.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 4/15/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import Foundation

public protocol FeedCell: class {
    static var reuseCellIdentifier: String { get set }
    weak var actionDelegate: ActionDelegate? { get set }
    static func heightUsing(feed: FeedData, commentPos: Int?) -> CGFloat
    func finalConfigurations(feed: FeedData, at position: Int)
}

public protocol EmptyFeedCell: class {
    static var reuseCellIdentifier: String { get set }
    static func height() -> CGFloat
    func configureEmptyCell(with tag: String)
}

public protocol ActionDelegate: class {
    func didPerform(action: String ,inside cell: UITableViewCell, on view: UIView)
}

