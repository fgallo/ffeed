//
//  FeedTableViewController.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 4/14/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import Foundation

public protocol FeedTableViewDataSource: class {
    func feedForIndex(_ index: Int) -> FeedData?
    func numberOfFeeds() -> Int
}

public protocol FeedTableViewDelegate: class {
    func didTapFeed(component: FeedCellsType, at index: Int, position: Int)
    func didPerform(action: String ,in component: FeedCellsType ,at indexPath: IndexPath, position: Int)
    func didSelectLink(label: UILabel, with url: URL)
    func willDisplayFeed(component: FeedCellsType, cell: UITableViewCell, at index: Int)
    func didDisplayFeed(component: FeedCellsType, cell: UITableViewCell, at index: Int)
}

public protocol FeedTableViewScrollDelegate: class {
    func scrollViewDidScroll(scrollView: UIScrollView)
}

extension FeedTableViewScrollDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView){}
}

extension FeedTableViewDelegate {
    func willDisplayFeed(component: FeedCellsType, cell: UITableViewCell, at index: Int){}
    func didDisplayFeed(component: FeedCellsType, cell: UITableViewCell, at index: Int){}
}

/// All cells that goes together with one of these enums should conform to FeedCell protocol
///
/// - postHeader: Post header of the feed, cell should conform to FeedHeaderView
/// - postDescription: Post description text of the feed, cell should conform to FeedDescriptionView
/// - postMedia: Post media, image automatic setting, cell should conform to FeedMediaView
/// - postLikes: Post Likes, cell should conform to FeedLikesView
/// - postComments: Post comments of the feed, cell shouls conform to FeedCommentView
/// - postCustom1: Custom cell type
/// - postCustom2: Custom cell type 2
/// - postCustom3: Custom cell type 3
public enum FeedCellsType {
    case postHeader
    case postDescription
    case postMedia
    case postLikes
    case postComments
    case postCustom1
    case postCustom2
    case postCustom3
}

public final class FeedTableViewController : NSObject {
    
    public typealias CellConfiguration = (FeedCellsType,FeedCell.Type)
    
    /// Set the class of the empty cell the feed controller should display if needed
    public var emptyCell : EmptyFeedCell.Type?
    
    /// A tag for configuration of the empty cell in varied states
    public var emptyCellTag: String?
    /// Set to true if want to display empty cell
    public var shouldDisplayEmptyCell: Bool = false
    
    /// FeedTableView that is going to be used by the feedController for display
    public var feedTableView: FeedTableView?
    /// According to the cell configurations available, use this property to define
    /// the structure of the feed
    public var feedConfiguration: [CellConfiguration]?
    
    /// Delegate to which the feed controller will pass the actions ocurred inside the feed
    public weak var delegate: FeedTableViewDelegate?
    /// Scroll delegate so the position of the content can be known
    public weak var scrollDelegate: FeedTableViewScrollDelegate?
    
    /// Data source that should provide the data according to FeedData protocol
    public weak var dataSource: FeedTableViewDataSource?
    /// Create an instance of this property if the feed configurations can be big. Highly recommended to activate.
    public var cacheSource: FeedCacheSource? {
        didSet {
            cacheSource?.feedTableViewController = self
        }
    }
    
    //MARK:-  Table Configurations
    
    var maxUsersLikeName: Int = 5
    var automaticImagePreviewEnabled = false
    
    fileprivate var startCommentsPosition: Int? {
        return feedConfiguration?.index(where: { (element) -> Bool in
            element.0 == FeedCellsType.postComments
        })
    }
    
    public func firstCommentPosition(feed: FeedData) -> Int? {
        var config = feedConfiguration!
        removeNilsFor(config: &config, feed: feed)
        let index = config.index(where: {$0.0 == FeedCellsType.postComments})
        
        return index
    }
    
    fileprivate func numberOfCommentsForFeed(feed: FeedData) -> Int {
        guard let feedConfiguration = feedConfiguration else {
            return 0
        }
        
        guard feedConfiguration.contains(where: { (element) -> Bool in
            return element.0 == FeedCellsType.postComments
        }) else {
            return 0
        }
        
        if let maxComments = feed.maxComments {
            return min(feed.commentsData?.count ?? 0, maxComments)
        } else {
            return feed.commentsData?.count ?? 0
        }
    }
    
    fileprivate func numberOfRowsFor(feed: FeedData) -> Int {
        guard let feedConfiguration = feedConfiguration else {
            return 0
        }
        
        var numberOfRows = 0
        
        numberOfRows += numberOfCommentsForFeed(feed: feed)
        
        for element in feedConfiguration {
            switch element.0 {
            case .postHeader:
                if feed.headerData != nil {
                    numberOfRows += 1
                }
            case .postDescription:
                if feed.descriptionData != nil {
                    numberOfRows += 1
                }
            case .postMedia:
                if feed.mediaData != nil {
                    numberOfRows += 1
                }
            case .postLikes:
                numberOfRows += 1
            case .postCustom1, .postCustom2, .postCustom3:
                numberOfRows += 1;
                
            default:
                break
            }
        }
        return numberOfRows
    }
    
    fileprivate func removeNilsFor(config: inout [CellConfiguration], feed: FeedData) {
        
        var elementsToRemove: [Int] = []
        for (i ,element) in config.enumerated() {
            switch element.0 {
            case .postHeader:
                if feed.headerData == nil {
                    elementsToRemove.append(i)
                }
            case .postComments:
                if feed.commentsData == nil {
                    elementsToRemove.append(i)
                }
            case .postDescription:
                if feed.descriptionData == nil {
                    elementsToRemove.append(i)
                }
            case .postMedia:
                if feed.mediaData == nil {
                    elementsToRemove.append(i)
                }
            default:
                break
            }
        }
        
        for i in elementsToRemove.reversed() {
            config.remove(at: i)
        }
    }
    
    internal func expandedConfiguration(feed: FeedData) -> [CellConfiguration]? {
        guard let feedConfiguration = feedConfiguration else {
            return nil
        }
        
        var expandedConfiguration = feedConfiguration
        let nComments = numberOfCommentsForFeed(feed: feed)
        
        if nComments > 0 {
            let commentConfig = expandedConfiguration[startCommentsPosition!]
            expandedConfiguration.insert(contentsOf: repeatElement(commentConfig, count: nComments - 1), at: startCommentsPosition!)
        }
        
        removeNilsFor(config: &expandedConfiguration, feed: feed)
        
        return expandedConfiguration
    }
    
    fileprivate func cellConfigFor(index: Int, feed: FeedData, updateCache: Bool? = false, section: Int? = 0) -> CellConfiguration? {
        guard let expandedConfig = expandedConfiguration(feed: feed) else {
            return nil
        }
        guard index < expandedConfig.count else {
            return nil
        }
        
        if let updateCache = updateCache, updateCache {
            cacheSource?.updateCacheAt(index: section!, with: expandedConfig)
        }
        
        return expandedConfig[index]
    }
    
    fileprivate func tryToGetCellConfig(feed: FeedData, indexPath: IndexPath) -> CellConfiguration? {
        if let cacheDataSource = cacheSource {
            if let cachedCellConfig = cacheDataSource.getCachedCellConfigFor(section: indexPath.section,
                                                                             row: indexPath.row) {
                return cachedCellConfig
            }
        }
        return cellConfigFor(index: indexPath.row, feed: feed,updateCache: true, section: indexPath.section)
    }
    
    fileprivate func tryToGetFirstCommentPosition(feed: FeedData, indexPath: IndexPath) -> Int {
        let position = firstCommentPosition(feed: feed) ?? 0
        cacheSource?.updateCommentPostitionAt(index: indexPath.section, with: position)
        return position
    }
    
    //MARK: - Label Links
    
    /// Set the types of links that the feed will support automatic highling
    ///
    /// - Parameter config: config is an array with the link types which support is desired
    public func setLabelLinkController(config: [LinkTypes]) {
        LabelLinkController.sharedInstance.config = config
    }
    
    /// Activate automatic link highlight on feed comments and on feed description
    public func activateLabelLinkDelegate() {
        LabelLinkController.sharedInstance.delegate = self
    }
    
    /// Deactivate automatic link highlighting
    public func deactivateLabelLinkDelegate() {
        LabelLinkController.sharedInstance.delegate = nil
    }
    
    //MARK: - Update Table View Methods
    
    /// Upate a feed section on the table view
    ///
    /// - Parameter index: index of the feed to be deleted
    public func updateFeed(at index: Int) {
        guard let tableView = feedTableView else {
            return
        }
        let indexSet = IndexSet(integer: index)
        
        tableView.reloadSections(indexSet, with: .automatic)
    }
    
    /// Update specific feeds components on feed at index
    ///
    /// - Parameters:
    ///   - type: Feed component type to be updated
    ///   - index: Index of feed to be updated
    public func updateComponent(type: FeedCellsType, at index: Int) {
        guard let tableView = feedTableView else {
            return
        }
        guard let feed = dataSource?.feedForIndex(index) else {
            return
        }
        guard let expandedConfig = expandedConfiguration(feed: feed) else {
            return
        }
        var indexes:[IndexPath]  = []
        for (componentIndex, (elementType, _)) in expandedConfig.enumerated() {
            if elementType == type {
                indexes.append(IndexPath(row: componentIndex, section: index))
            }
        }
        tableView.reloadRows(at: indexes, with:.automatic)
    }
    
    /// Update a single component on Feed at index. It is required to specify the exact position of the item.
    /// If it is a post comment, only the relative position in relation to the first comment is necessary
    ///
    /// - Parameters:
    ///   - type: Feed component type to be updated
    ///   - index: Index of feed to be updated
    ///   - position: Position of item on feed. If it is a comment, relative position to the first comment suffices. If it is another component and the feed configuration has only one of it, zero can be used.
    public func updateSingleComponent(type: FeedCellsType, index: Int, position: Int){
        guard let tableView = feedTableView else {
            return
        }
        guard let feed = dataSource?.feedForIndex(index) else {
            return
        }
        guard let expandedConfig = expandedConfiguration(feed: feed) else {
            return
        }
        guard let firstOfThatType = expandedConfig.index(where: {$0.0 == type}) else {
            return
        }
        let firstOfThatTypeInt = -firstOfThatType.distance(to: expandedConfig.startIndex)
        let positionOnTable = firstOfThatTypeInt+position
        guard expandedConfig[positionOnTable].0 == type else {
            return
        }
        let indexPath = IndexPath(row: positionOnTable, section: index)
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    /// Insert Comment
    ///
    /// - Parameters:
    ///   - onEnd: True if comment should be inserted on the end of the feed
    ///   - index: index of feed where the comment shall be inserted
    public func insertComment(onEnd: Bool, at index: Int) {
        guard let tableView = feedTableView else {
            return
        }
        guard let feed = dataSource?.feedForIndex(index) else {
            return
        }
        guard var expandedConfig = expandedConfiguration(feed: feed) else {
            return
        }
        var commentPos: Int
        
        if onEnd {
            expandedConfig = expandedConfig.reversed()
            commentPos = expandedConfig.count - 2
        } else {
            if let i = expandedConfig.index(where: { $0.0 == FeedCellsType.postComments}) {
                commentPos = -i.distance(to: expandedConfig.startIndex)
            } else {
                commentPos = expandedConfig.count - 2
            }
        }
        let indexPath = IndexPath(row: commentPos, section: index)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
}

//MARK: - TableView Data Source

extension FeedTableViewController: UITableViewDataSource {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        guard let dataSource = dataSource else {
            return 0
        }
        if shouldDisplayEmptyCell {
            return 1
        }
        return dataSource.numberOfFeeds()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let dataSource = dataSource else {
            return 0
        }
        if shouldDisplayEmptyCell {
            return 1
        }
        let feed = dataSource.feedForIndex(section)!
        return numberOfRowsFor(feed: feed)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if shouldDisplayEmptyCell && emptyCell != nil  {
            return emptyCell!.height()
        }
        
        let feed = dataSource!.feedForIndex(indexPath.section)!
        guard let cellConfig = tryToGetCellConfig(feed: feed, indexPath: indexPath) else {
            return 0
        }
        let firstCommentPos = tryToGetFirstCommentPosition(feed: feed, indexPath: indexPath)
        
        let height = cellConfig.1.heightUsing(feed: feed,
                                              commentPos: indexPath.row - firstCommentPos)
        
        return height
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if shouldDisplayEmptyCell && (emptyCell != nil) {
            let cell = tableView.dequeueReusableCell(
                withIdentifier: emptyCell!.reuseCellIdentifier,
                for: indexPath)
            if let cell = cell as? EmptyFeedCell {
                cell.configureEmptyCell(with: self.emptyCellTag ?? "")
                return cell as! UITableViewCell
            }
        }
        
        let feed = dataSource!.feedForIndex(indexPath.section)!
        
        guard let cellConfig = tryToGetCellConfig(feed: feed, indexPath: indexPath) else {
            return UITableViewCell()
        }
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellConfig.1.reuseCellIdentifier, for: indexPath)
        
        switch cellConfig.0 {
        case .postComments:
            if var commentCell = cell as? FeedCommentView ,
                let commentData = feed.commentsData?[indexPath.row - firstCommentPosition(feed: feed)!] {
                commentCell.configure(data: commentData)
                cell = commentCell as! UITableViewCell
            }
        case .postHeader:
            if let headerCell = cell as? FeedHeaderView,
                let headerData = feed.headerData {
                headerCell.configure(data: headerData)
                cell = headerCell as! UITableViewCell
            }
        case .postLikes:
            if let likesCell = cell as? FeedLikesView,
                let likesData = feed.likesData {
                likesCell.configure(data: likesData, maxUsersLikeName: maxUsersLikeName)
                cell = likesCell as! UITableViewCell
            }
        case .postMedia:
            if let mediaCell = cell as? FeedMediaView,
                let mediaData = feed.mediaData {
                mediaCell.configure(data: mediaData, previewEnabled: automaticImagePreviewEnabled)
                cell = mediaCell as! UITableViewCell
            }
            break
        case .postDescription:
            if var descriptionCell = cell as? FeedDescriptionView,
                let descriptionData = feed.descriptionData {
                descriptionCell.configure(data: descriptionData)
                cell = descriptionCell as! UITableViewCell
            }
        default:
            break
        }
        
        if let feedCell = cell as? FeedCell {
            let index = indexPath.row - tryToGetFirstCommentPosition(feed: feed, indexPath: indexPath)
            feedCell.finalConfigurations(feed: feed, at: index)
            feedCell.actionDelegate = self
            cell = feedCell as! UITableViewCell
        }
        return cell
    }
    
}

//MARK: - TableView Delegate

extension FeedTableViewController: UITableViewDelegate {
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollDelegate?.scrollViewDidScroll(scrollView: scrollView)
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if shouldDisplayEmptyCell { return }
        let feed = dataSource!.feedForIndex(indexPath.section)!
        guard let cellConfig = tryToGetCellConfig(feed: feed, indexPath: indexPath) else {
            return
        }
        var position: Int = indexPath.row
        if cellConfig.0 == .postComments {
            let firstCommentPos = tryToGetFirstCommentPosition(feed: feed, indexPath: indexPath)
            position = indexPath.row - firstCommentPos
        }
        
        delegate?.didTapFeed(component: cellConfig.0, at: indexPath.section, position: position)
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if shouldDisplayEmptyCell { return }
        guard let feed = dataSource!.feedForIndex(indexPath.section) else { return }
        guard let cellConfig = tryToGetCellConfig(feed: feed, indexPath: indexPath) else {
            return
        }
        delegate?.willDisplayFeed(component: cellConfig.0, cell: cell, at: indexPath.section)
    }
    
    public func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if shouldDisplayEmptyCell { return }
        guard let feed = dataSource!.feedForIndex(indexPath.section) else { return }
        guard let cellConfig = tryToGetCellConfig(feed: feed, indexPath: indexPath) else {
            return
        }
        delegate?.didDisplayFeed(component: cellConfig.0, cell: cell, at: indexPath.section)
    }
    
}

//MARK: - Action Delegate

extension FeedTableViewController: ActionDelegate {
    
    public func didPerform(action: String ,inside cell: UITableViewCell, on view: UIView) {
        guard let indexPath = feedTableView?.indexPath(for: cell) else {
            return
        }
        guard let feed = dataSource!.feedForIndex(indexPath.section) else { return }
        guard let cellConfig = tryToGetCellConfig(feed: feed, indexPath: indexPath) else {
            return
        }
        var position = -1
        if cellConfig.0 == .postComments {
            let firstCommentPos = tryToGetFirstCommentPosition(feed: feed, indexPath: indexPath)
            position = indexPath.row - firstCommentPos
        }
        delegate?.didPerform(action: action, in: cellConfig.0, at: indexPath, position: position)
    }
}

//MARK: - Label Link Delegate

extension FeedTableViewController: LabelLinkControllerDelegate {
    
    func didSelectLink(label: UILabel, url: URL) {
        delegate?.didSelectLink(label: label, with: url)
    }
}

