//
//  FeedTableView.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 4/14/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import UIKit

public class FeedTableView: UITableView {
    
    public var controller: FeedTableViewController? {
        didSet {
            super.delegate = controller
            super.dataSource = controller
        }
    }
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        controller = nil
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        controller = nil
    }
}
