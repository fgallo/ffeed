//
//  FeedDescriptionView.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 4/15/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import Foundation
import TTTAttributedLabel

public protocol FeedDescriptionView {
    var descriptionLabel: TTTAttributedLabel? { get set }
}

extension FeedDescriptionView {
    
    private var labelLinkController: LabelLinkController {
        return LabelLinkController.sharedInstance
    }
    
    mutating func configure(data: FeedDescriptionData) {
        guard descriptionLabel != nil else {
            return
        }
        
        if let descriptionBase64 = data.descriptionBase64 {
            descriptionLabel?.text = FeedHelper.decodeString(string64: descriptionBase64)
        } else {
            descriptionLabel!.text = data.description
        }
        for mention in data.mentions {
            labelLinkController.addLinkToFirst(label: &descriptionLabel!, mention: mention)
        }
        labelLinkController.createLinks(label: &descriptionLabel!)
    }
}
