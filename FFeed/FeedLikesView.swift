//
//  FeedLikesView.swift
//  FFeed
//
//  Created by Felipe Figueiredo on 4/15/17.
//  Copyright © 2017 Felipe Figueiredo. All rights reserved.
//

import Foundation

public protocol FeedLikesView {
    var likeLabel: UILabel? { get set }
    var likeLabelFormat: LikeLabelFormat? { get set }
    var likeName: String? { get set }
}

public enum LikeLabelFormat {
    case onlyNumber
    case namesAndNumber
}

extension FeedLikesView {
    func configure(data: FeedLikesData, maxUsersLikeName: Int) {
        likeLabel?.text = ""
        
        guard  let likeLabel = likeLabel else {
            return
        }
        
        guard let likeLabelFormat = likeLabelFormat else {
            return
        }
        
        guard let nLikes = data.numberOfLikes else {
            return
        }
        
        switch likeLabelFormat {
        case .onlyNumber:
            likeLabel.text = String(nLikes) + " " + (likeName ?? "")
            
        case .namesAndNumber:
            guard let users = data.userLikeNames else {
                return
            }
            
            if nLikes > maxUsersLikeName {
                for i in 0..<maxUsersLikeName - 1 {
                    likeLabel.text? = likeLabel.text! + users[i] + ", "
                }
                likeLabel.text?.append(users[maxUsersLikeName - 1])
                
                likeLabel.text!.append("e mais \(maxUsersLikeName - users.count) " + (likeName ?? "curtidas"))
                
            } else {
                likeLabel.text = users.joined(separator: ", ")
            }
        }
    }
}
